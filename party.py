# This file is part of Tryton.  The COPYRIGHT file at the top level
# of this repository contains the full copyright notices and license terms.
from trytond.model import ModelView, ModelSQL, fields
from trytond.modules.party.party import STATES, DEPENDS
from trytond.pyson import Eval


class Party(ModelSQL, ModelView):
    _name = 'party.party'

    contract_support = fields.Property(fields.Many2One('contract.contract',
            'Support Contract', context={
                'party': Eval('active_id'),
                'pricelist': Eval('pricelist_sale'),
            }, domain=[
                ('state', '=', 'active'),
                ('company', '=', Eval('company')),
                ('party_invoice', '=', Eval('active_id'))
            ], select=1, states=STATES,
            depends=DEPENDS+['company', 'active_id']))

Party()
