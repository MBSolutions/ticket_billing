# This file is part of Tryton. The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import ModelView, ModelSQL, fields


class Line(ModelSQL, ModelView):
    _name = 'timesheet.line'

    def get_work(self, line):
        res = super(Line, self).get_work(line)
        if line.work.ticket_works:
            res = line.work.ticket_works[0]
        return res

Line()


class TimesheetWork(ModelSQL, ModelView):
    _name = 'timesheet.work'

    # There should be a constraint that there must be only one record in this
    # One2Many
    ticket_works = fields.One2Many('ticket.work', 'work', 'Ticket Work')

TimesheetWork()
